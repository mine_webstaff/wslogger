# -*- coding: utf-8 -*-

import logging


class WSLogger:
    def __init__(self, logname, debug_mode=logging.DEBUG):
        self.logger = logging.getLogger(logname)
        self.logger.setLevel(debug_mode)
        self.stream_handler = logging.StreamHandler()
        self.stream_handler.setLevel(debug_mode)
        self.handler_format = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        self.stream_handler.setFormatter(self.handler_format)
        self.file_handler = logging.FileHandler(logname + '.log', 'w', encoding='utf-8')
        self.file_handler.setFormatter(self.handler_format)
        self.logger.addHandler(self.stream_handler)
        self.logger.addHandler(self.file_handler)

    def get_logger(self):
        return self.logger
